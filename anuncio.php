<?php
    require 'includes/funciones.php';
    incluirTemplate('header');

    //RECIBIR Y VALIDAR EL ID
    $id = $_GET['id'];

    $id = filter_var($id, FILTER_VALIDATE_INT);

    //redireccionar 
    if (!$id) {
        header('Location: /');
    }

    //Importar la conexion a la DataBase
    include 'includes/config/database.php';
    $db = conectarDB();

    //consultar en la base de datos 

    $query = " SELECT * FROM propiedades WHERE id = ${id}";

    //Obtener los datos
    $resultados = mysqli_query($db, $query);

    if (!$resultados->num_rows) {
        header('Location: /');
    } 

    $propiedad = mysqli_fetch_assoc($resultados);

?>

    <main class="contenedor seccion contenido-centrado">
        <h1><?php echo $propiedad['titulo'] ?></h1>

        <img class="imagen-anuncio" loadiing="lazy" src="/imagenes/<?php echo $propiedad['imagen'] ?>" alt="Imagen de la propiedad">

        <div class="resumen-propiedad">
            <p class="precio">$ <?php echo $propiedad['precio'] ?></p>

            <ul class="iconos-caracteristicas">
                <li>
                    <img class="icono" loading="lazy" src="build/img/icono_wc.svg" alt="Icono de baños">
                    <p><?php echo $propiedad['wc'] ?></p>
                </li>
                <li>
                    <img class="icono" loading="lazy" src="build/img/icono_estacionamiento.svg" alt="Icono de Estacionamiento">
                    <p><?php echo $propiedad['estacionamiento'] ?></p>
                </li>
                <li>
                    <img class="icono" loading="lazy" src="build/img/icono_dormitorio.svg" alt="Icono de Habitaciones">
                    <p><?php echo $propiedad['habitaciones'] ?></p>
                </li>
            </ul>

            <p><?php echo $propiedad['descripcion'] ?>
            </p>




        </div>
    </main>

     

<?php 

    mysqli_close($db);
    incluirTemplate('footer');

?>
 