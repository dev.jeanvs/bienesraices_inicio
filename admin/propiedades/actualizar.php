<?php

require '../../includes/funciones.php';

$auth = estaAutenticado();

if (!$auth) {
    header('Location: /');
}

//Validar id de la propiedad
$id = $_GET['id'];
$id = filter_var($id, FILTER_VALIDATE_INT);

if (!$id) {
    header('Location :/admin');
}

//Base de Datos

require '../../includes/config/database.php';

$db = conectarDB();

//Obtener los datos de la propiedad 
$consulta = "SELECT * FROM propiedades WHERE id = ${id}";
$resultado = mysqli_query($db, $consulta);
$propiedad = mysqli_fetch_assoc($resultado);

//consultar para obtener los vendedores de la base de datos
$consulta = "SELECT * FROM vendedores";
$resultado = mysqli_query($db, $consulta);


//Arreglo con mensajes de errorres 
$errores = [];

$titulo = $propiedad['titulo'];
$precio = $propiedad['precio'];
$descripcion = $propiedad['descripcion'];
$habitaciones = $propiedad['habitaciones'];
$wc = $propiedad['wc'];
$estacionamiento = $propiedad['estacionamiento'];
$vendedorId = $propiedad['vendedorId'];
$imagenPropiedad = $propiedad['imagen'];

//Ejecutar el codigo despues de que el usuario envia el formulario
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //
    //  echo "<pre>";  
    //  var_dump($_POST);
    //  echo "</pre>";

    //Files es para ver mas informacion de la imagenes 
    // echo "<pre>";
    // var_dump($_FILES);
    // echo "</pre>";

    //El post captura los valores del formulario  
    //La funcion mysqli_real_escape_string() valida que no se intecte codigo mysql

    $titulo = mysqli_real_escape_string($db, $_POST['titulo']);
    $precio = mysqli_real_escape_string($db, $_POST['precio']);
    $descripcion = mysqli_real_escape_string($db, $_POST['descripcion']);
    $habitaciones = mysqli_real_escape_string($db, $_POST['habitaciones']);
    $wc = mysqli_real_escape_string($db, $_POST['wc']);
    $estacionamiento = mysqli_real_escape_string($db, $_POST['estacionamiento']);
    $vendedorId = mysqli_real_escape_string($db, $_POST['vendedorId']);
    $creado = date('Y/m/d');

    //Asignar files hacia una variable 
    $imagen = $_FILES['imagen_'];


    if (!$titulo) {
        $errores[] = "Debes añadir un título";
    }

    if (!$precio) {
        $errores[] = "El precio es obligatorio";
    }

    if (strlen($descripcion) < 50) {
        $errores[] = "Debes añadir una descripcion de almenos 50 caracteres";
    }

    if (!$habitaciones) {
        $errores[] = "El número habitaciones es obligatorio";
    }

    if (!$wc) {
        $errores[] = "El número de baños es obligatorio";
    }

    if (!$estacionamiento) {
        $errores[] = "El número de estacionamientos es obligatorio";
    }

    if (!$vendedorId) {
        $errores[] = "Seleccione un vendedor";
    }

    // validar por tamaño de imagen

    $medida = 1000 * 1000;

    if ($imagen['size'] > $medida) {
        $errores[] = 'La imagen es muy grande';
    }

    // echo "<pre>";
    // var_dump($errores);
    // echo "</pre>";

    // exit;

    //revisar que el arreglo de errores esté vacio 

    if (empty($errores)) {
        // // SUBIDA DE ARCHIVOS//


        //Crear Carpeta
        $carpetaImagenes = '../../imagenes/';

        if (!is_dir($carpetaImagenes)) {
            mkdir($carpetaImagenes);
        }

        $nombreImagen = ' ';

        if ($imagen['name']) {
            // echo "Si hay una imagen";
            //Eliminar la imagen previa 
            unlink($carpetaImagenes . $propiedad['imagen']);

            // Generar un nombre unico  
            $nombreImagen = md5(uniqid(rand(), true)) . ".jpg";

            // Subir la imagen 
            move_uploaded_file($imagen['tmp_name'], $carpetaImagenes . $nombreImagen);
        } else {
            $nombreImagen = $propiedad['imagen'];
        }


        //Insertar en la base de datos //
        $query = " UPDATE propiedades SET titulo = '${titulo}', precio = ${precio},
        imagen = '${nombreImagen}', descripcion= '${descripcion}', habitaciones = ${habitaciones},
        wc= ${wc}, estacionamiento= ${estacionamiento}, vendedorId= ${vendedorId} WHERE id =${id} ";

        // echo $query;

        // exit;

        $resultado = mysqli_query($db, $query);

        if ($resultado) {
            echo "Insertado Correctamente";

            header('location: /admin?resultado=2');
        } else {
            echo "Error";
        }
    }
}


incluirTemplate('header');

?>

<main class="contenedor seccion">
    <h1>Actualizar propiedades</h1>

    <a href="/admin" class="boton boton-verde">Volver</a>

    <?php foreach ($errores as $error) : ?>
        <div class="alerta error">
            <?php echo $error; ?>
        </div>
    <?php endforeach; ?>

    <form class="formulario" method="POST" enctype="multipart/form-data">
        <fieldset>
            <legend>Información general</legend>

            <label for="titulo">Título: </label>
            <input name="titulo" type="text" id="titulo" placeholder="Título propiedad" value="<?php echo $titulo; ?>">

            <label for="precio">Precio: </label>
            <input name="precio" type="number" id="precio" placeholder="Precio propiedad" value="<?php echo $precio; ?>">

            <label for="imagen">Imagen: </label>
            <input name="imagen " type="file" id="imagen" accept="image/jpeg, image/png">

            <img class="imagen-pequeña" src="/imagenes/<?php echo $imagenPropiedad; ?>" alt="Imagen de la propiedad">

            <label for="descripcion">Descripcion: </label>
            <textarea name="descripcion" id="descripcion"><?php echo $descripcion; ?></textarea>

        </fieldset>

        <fieldset>
            <legend>Información propiedad</legend>

            <label for="habitaciones">Habitaciones: </label>
            <input name="habitaciones" type="number" id="habitaciones" placeholder="Ej: 3" min="1" max="9" value="<?php echo $habitaciones; ?>">

            <label for="wc">Baños: </label>
            <input name="wc" type="number" id="wc" placeholder="Ej: 3" min="1" max="9" value="<?php echo $wc; ?>">

            <label for="estacionamiento">Estacionamiento: </label>
            <input name="estacionamiento" type="number" id="estacionamiento" placeholder="Ej: 3" min="1" max="9" value="<?php echo $estacionamiento; ?>">

        </fieldset>

        <fieldset>
            <legend>Vendedor </legend>

            <select name="vendedorId">
                <option value="">--- Seleccione --</option>
                <?php while ($vendedor = mysqli_fetch_assoc($resultado)) : ?>
                    <option <?php echo $vendedorId === $vendedor['id'] ? 'selected' : ''; ?> value="<?php echo $vendedor['id']; ?>"><?php echo $vendedor['nombre'] . " " . $vendedor['apellido'] ?></option>
                <?php endwhile; ?>

            </select>
        </fieldset>

        <input type="submit" value="Actualizar" class="boton boton-verde">
    </form>

</main>

<?php
incluirTemplate('footer');
?>