<?php  

//Impotar la conexion
require __DIR__. '/../config/database.php';

$db = conectarDB();

//Consultar

$query = " SELECT * FROM propiedades LIMIT ${limite}";

//Leer resultados 

$resultados = mysqli_query($db, $query);

?>


<div class="contenedor-anuncios">
<?php while ( $propiedad = mysqli_fetch_assoc($resultados) ) :?>
    <div class="anuncio">
        
        <img src="/imagenes/<?php echo $propiedad['imagen'] ?>" alt="Anuncio" loading="lazy">

        <div class="contenido-anuncio">
            <h3><?php echo $propiedad['titulo'] ?></h3>
            <p><?php echo $propiedad['descripcion'] ?></p>
            <p class="precio">$<?php echo $propiedad['precio'] ?></p>

            <ul class="iconos-caracteristicas">
                <li>
                    <img class="icono " loading="lazy" src="build/img/icono_wc.svg" alt="Icono de baños">
                    <p><?php echo $propiedad['wc'] ?></p>
                </li>
                <li>
                    <img class="icono" loading="lazy" src="build/img/icono_estacionamiento.svg" alt="Icono de Estacionamiento">
                    <p><?php echo $propiedad['estacionamiento'] ?></p>
                </li>
                <li>
                    <img class="icono" loading="lazy" src="build/img/icono_dormitorio.svg" alt="Icono de Habitaciones">
                    <p><?php echo $propiedad['habitaciones'] ?></p>
                </li>
            </ul>

            <a href="anuncio.php?id=<?php echo $propiedad['id'] ?>" class="boton-amarillo-block">
                Ver Propiedad
            </a>
        </div> <!-- Contenido Anuncio-->
    </div> <!-- Anuncio-->

    <?php endwhile; ?>
</div>
<!--Contenedor de Anuncio-->


<?php 
    //CERRAR LA CONEXIOn
    mysqli_close($db);

?>