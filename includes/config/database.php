<?php

function conectarDB() : mysqli {
    $db = mysqli_connect('localhost', 'root', '1250', 'bienes_raices');

    if(!$db) {
        echo "Erro de conexión a la Base de Datos";
        exit;
    }

    return $db;
}