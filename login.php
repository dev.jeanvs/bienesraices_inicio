<?php

    //Importar Conexion en la base de datos
    require 'includes/config/database.php';
    $db = conectarDB();


    // Autenticar el usuario
    $errores = [];

    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        $email = mysqli_real_escape_string($db, filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL));
        $password = mysqli_real_escape_string($db,  $_POST['password']);

        if (!$email) {
            $errores[] = "El email es obligatorio o no es válido";
        }

        if (!$password) {
            $errores[] = "El passsword es obligatorio ";
        }

        if (empty($errores)) {
            //revisar si el usuario existe.
            $query = " SELECT * FROM usuarios WHERE email = '${email}' ";
            $resultado = mysqli_query($db,$query);


            if ($resultado->num_rows) {
                //revisar si el password es correcto 
                $usuario = mysqli_fetch_assoc($resultado);

                //Verificar si el password es correcto o no 
                $auth = password_verify($password, $usuario['password']);


                if ($auth) {
                    //El usuario está autencticado 
                    session_start();

                    //Llenar el arreglo de la sesión 
                    $_SESSION['usuario'] = $usuario['email'];
                    $_SESSION['login'] = true;

                    header('Location: /admin');

                } else {
                    $errores[] = 'La contraseña es incorrecta';
                }
            }else {
                $errores[] = "El usuario no existe";
            }
        }
    }

    // Incluye el header
    require 'includes/funciones.php';
    incluirTemplate('header');
?>

    <main class="contenedor seccion contenido-centrado">
        <h1>Iniciar Sesión</h1>

        <?php foreach($errores as $error): ?>
            <div class="alerta error">
                 <?php echo $error; ?>
            </div>
        <?php endforeach; ?>

        <form method="POST" class="formulario">
            <fieldset>
                <legend>Accede en modo administrador</legend>

                <label for="email">Correo</label>
                <input type="email" name="email" placeholder="Tu email" id="email" required>

                <label for="password">Contraseña</label>
                <input type="password" name="password" placeholder="Contraseña" id="password" required>

                <input type="submit" value="Continuar" class="boton-verde">

            </fieldset>
        </form>
    </main>

<?php
    incluirTemplate('footer');
?>